-- Para correr el proyecto:
--  1- Elige un testbench de la parte inferior del archivo
--  2- Ejecuta la expresión "test <testbench>"
--  Para las estadísticas, por ejemplo, el comando seria "test testbench_stats"

----------------------------------------------
-- Tipos Generales
----------------------------------------------
type QuestionID = Int
type LetraRespuesta = Char
type StudentID = String
type TestID = String
data TipoRespuesta = Correcta | EnBlanco | Erronea
type ModeloID = String

type Respuesta = (QuestionID, LetraRespuesta)
type Pregunta = (QuestionID, [LetraRespuesta], LetraRespuesta)
type Modelo = (ModeloID, [QuestionID])

type Preguntas = [Pregunta]
type Respuestas = [Respuesta]
type Modelos = [Modelo]

type RespuestaTest = (StudentID, TestID, Respuestas, ModeloID)
type Test = (TestID, Preguntas, Modelos)

----------------------------------------------
-- Correccion
----------------------------------------------
type ScoreOver10 = Float
type TotalScore = Float
type Correccion = (StudentID, TotalScore, ScoreOver10)

corrige :: Test -> RespuestaTest -> Correccion
corrige (testID, preguntas, modelos) (s, respuestaID, respuestas, modeloID)
 | testID == respuestaID = let preguntasDeModelo = (construyeModelo preguntas modelos modeloID)
                               total = (corrigeListaPreguntas preguntasDeModelo respuestas)
                               ponderado = sobre10 total (fromIntegral(length preguntas))
                           in (s, total, ponderado)

-- Dadas listas de modelos y preguntas, construye una lista de preguntas ordenadas
-- de acuerdo con el modelo, para poder ser corregidas.
construyeModelo :: Preguntas -> Modelos -> ModeloID -> Preguntas
construyeModelo p ms mid =
 let modelo = (encuentraModelo mid ms)
 in (ordenaPreguntas p modelo)

-- Dada una lista de modelos, encuentra el modelo apropiado
encuentraModelo :: ModeloID -> [Modelo] -> Modelo
-- No-definido: encuentraModelo id [] = undefined
encuentraModelo objetivo modelos = head (filter ((==objetivo).fst) modelos)

-- Ordena una lista de preguntas de acuerdo con un modelo
ordenaPreguntas :: Preguntas -> Modelo -> Preguntas
ordenaPreguntas elems (m, []) = []
ordenaPreguntas elems (m, (id:ids)) = (elems !! id):(ordenaPreguntas elems (m,ids))

-- Devuelve la puntuación no-normalizada de una lista de respuestas sobre una lista de preguntas
-- previamente ordenada según un modelo
corrigeListaPreguntas :: Preguntas -> Respuestas -> Float
corrigeListaPreguntas [] [] = 0.0
corrigeListaPreguntas (p:ps) (r:rs)
 | (esCorrecta p r) = (corrigeListaPreguntas ps rs) + 1
 | (enBlanco p r) = (corrigeListaPreguntas ps rs)
 | (otherwise) = (corrigeListaPreguntas ps rs) - ((/) 1.0 ((alternativas p) - 1))

-- Numero de alternativas en una pregunta
alternativas :: Pregunta -> Float
alternativas (_, xs, _) = fromIntegral (length xs)

esCorrecta :: Pregunta -> Respuesta -> Bool
esCorrecta (pID, _, pL) (rID, rL) = ((pID == rID) && (pL == rL))

enBlanco :: Pregunta -> Respuesta -> Bool
enBlanco (pID, _, pL) (rID, rL) = ((pID == rID) && (rL == ' '))

-- Normaliza una puntuación
sobre10 :: Float -> Float -> Float
sobre10 x y = ((/) x y) * 10.0

----------------------------------------------
-- Estadisticas
----------------------------------------------

type QuestionCount = (QuestionID, NumPeople)
type MeanPopulationScore = Float
type MeanPopulationAnsweredQuestions = Float
type NumPeople = Int
-- Frecuencias absolutas de correcto, en blanco y fallido para una pregunta
type AbsoluteFrequency = (Int, Int, Int)
type RelativeFrequency = (Float, Float, Float)
type Frequency = (AbsoluteFrequency, RelativeFrequency)

-- Means :: (puntuación media, número medio de respuestas respondidas)
type Means = (MeanPopulationScore, MeanPopulationAnsweredQuestions)
-- PeoplePartition :: (suspensos, aprobados, notables, sobresalientes)
type PeoplePartition = (NumPeople, NumPeople, NumPeople, NumPeople)
-- Frequencies :: [(correctas, erróneas, en blanco)]
type Frequencies = [Frequency]
-- RelevantQuestions :: (mejores resultados, peores, más en blanco)
type RelevantQuestions = (QuestionID, QuestionID, QuestionID)

type Estadisticas = (Means, PeoplePartition, Frequencies, RelevantQuestions)

estadisticas :: Test -> [RespuestaTest] -> Estadisticas
estadisticas test respuestas =
  let correcciones = map (corrige test ) respuestas
      (correctas, blanco, incorrectas) = conteos test respuestas
  in (medias correcciones (correctas, blanco, incorrectas), particiones correcciones, frecuencias (correctas, blanco, incorrectas), relevantes (correctas, blanco, incorrectas))

--------------------
-- Funciones principales para calcular estadisticas
--------------------

-- Devuelve una tupla cuyo primer elemento es la puntuacion media,
-- y el segundo el numero medio de preguntas respondidas
medias :: [Correccion] -> ([QuestionCount], [QuestionCount], [QuestionCount]) -> Means
medias correcciones (correctas, blanco, incorrectas) =
  let numPreguntas = fromIntegral (length blanco)
      numAlumnos = fromIntegral (length correcciones)
      puntTotal = foldr (\(id, s, s10) acc -> acc + s10) 0 correcciones
      puntMedia = ((/) puntTotal numAlumnos)
      noRespondidas = fromIntegral (foldr (\(i, v) acc -> acc + v) 0 blanco)
      -- Las preguntas respondidas son el número total de preguntas
      -- menos las no respondidas
      respondidas = (numAlumnos * numPreguntas) - noRespondidas
  in  (puntMedia, ((/) respondidas numAlumnos))

-- Simplemente vamos sumando por cada corrección, sumando en el elemento correspondiente de la tupla
particiones :: [Correccion] -> PeoplePartition
particiones correcciones =
  foldr (\(id, s, s10) (ss, ap, not, sob) ->
    if s10 < 5 then (ss + 1, ap, not, sob) else
    if s10 >= 5 && s10 < 7 then (ss, ap + 1, not, sob) else
    if s10 >= 7 && s10 < 9 then (ss, ap, not + 1, sob) else
    (ss, ap, not, sob + 1))
    (0, 0, 0, 0) correcciones

-- Devuelve una lista de frecuencias (pares (id, valor)) ordenada por preguntas.
-- Cada elemento de la lista es una tupla de dos elementos (freq. absolutas o relativas).
-- Cada elemento de la tupla es a su vez una tupla de tres elementos con las frecuencias
-- absolutas o relativas de las respuestas en correctas, en blanco, o erróneas
frecuencias :: ([QuestionCount], [QuestionCount], [QuestionCount]) -> Frequencies
frecuencias (correctas, blanco, incorrectas) =
  let extractSecond xs = [y | (x, y) <- xs]
      absolutas = zip3 (extractSecond correctas) (extractSecond blanco) (extractSecond incorrectas)
      -- El numero de alumnos es la suma de frecuencias por cada pregunta
      numAlumnos = fromIntegral ((\(x, y, z) -> x + y + z) (head absolutas))
      relativizar count = ((/) (fromIntegral count) numAlumnos)
      relativas = map (\(c, b, i) -> (relativizar c, relativizar b, relativizar i)) absolutas
  in zip absolutas relativas

-- Se define una función auziliar que calcula el indice del máximo valor
-- en una lista de tuples de (indice, valor), y se aplica a los conteos
relevantes :: ([QuestionCount], [QuestionCount], [QuestionCount]) -> RelevantQuestions
relevantes (correctas, blanco, incorrectas) =
  let maximo xs = fst (foldr (\(i, v) (loci, locv) -> if locv < v then (i, v) else (loci, locv)) (0, 0) xs)
  in (maximo correctas, maximo blanco, maximo incorrectas)

-----------------------
-- conteos
-----------------------

-- Devuelve una tupla de 3 listas, en cada elemento del tuple hay una lista con pares (pregunta/cuenta)
-- por ejemplo, puede devolver ([(1, 2), (2, 0)], [(1, 3), (2, 4)], [(1, 0), (2, 1)]),
-- lo que quiere decir que 2 personas acertaron la primera pregunta y 0 personas la segunda,
-- 3 personas dejaron en blanco la primera pregunta y 4 la segunda...
conteos :: Test -> [RespuestaTest] -> ([QuestionCount], [QuestionCount], [QuestionCount])
conteos (testID, preguntas, modelosBase) respuestas =
  let -- Por cada alumno, construimos su test con su modelo
      modelos = map (\(s, t, r, m) -> construyeModelo preguntas modelosBase m) respuestas
      -- Por cada alumno, contamos las correctas, en blanco y fallos
      cuentasAlumnos = map (\((s, t, r, m), y) -> cuentaAlumno r y) (zip respuestas modelos)
      -- Ahora unimos todo para crear una sola lista de cuentas
      vacia :: [(Int, Int)]
      vacia = [(x, 0)| x <- [0..length respuestas]]
      acc = (vacia, vacia, vacia)
      cuentasTotales = foldr (\(cor, bl, fa) (accCor, accBl, accFa) -> (colapsa cor accCor, colapsa bl accBl, colapsa fa accFa)) (vacia, vacia, vacia) cuentasAlumnos
  in cuentasTotales

-- Análoga a la funcion anterior, pero con las respuestas de un solo alumno.
-- Por cada pregunta, cuenta si la respuesta es correcta, si está en blanco, o si es errónea
cuentaAlumno :: Respuestas -> Preguntas -> ([QuestionCount], [QuestionCount], [QuestionCount])
cuentaAlumno [] [] = ([], [], [])
cuentaAlumno ((rID, rL):rs) ((pID, pLs, pL):ps)
  | (esCorrecta (pID, pLs, pL) (rID, rL)) =
      let (correctas, blanco, fallidas) = cuentaAlumno rs ps
      in  ((pID, 1):correctas, (pID, 0):blanco, (pID, 0):fallidas)
  | (enBlanco (pID, pLs, pL) (rID, rL)) =
      let (correctas, blanco, fallidas) = cuentaAlumno rs ps
      in  ((pID, 0):correctas, (pID, 1):blanco, (pID, 0):fallidas)
  | otherwise =
      let (correctas, blanco, fallidas) = cuentaAlumno rs ps
      in  ((pID, 0):correctas, (pID, 0):blanco, (pID, 1):fallidas)

-- Aplica un resultado parcial a la cuenta que llevamos
colapsa :: [QuestionCount] -> [QuestionCount] -> [QuestionCount]
colapsa xs [] = []
colapsa xs ((ndx, res):xss) = (ndx, ((encuentraValor ndx xs) + res)):(colapsa xs xss)

-- Encuentra el valor del elemento de indice ndx en una lista de tuplas (indice, valor)
encuentraValor :: Int -> [(Int, t)] -> t
encuentraValor ndx xs = head [v | (i, v) <- xs, i == ndx]
----------------------------------------------
-- Tests
----------------------------------------------

-- To run:
--  1- Pick a testbench
--  2- Run test <testbench>

-- Auxiliary Functions for Testing
p :: QuestionID -> LetraRespuesta -> Pregunta
p x y = (x, ['a', 'b', 'c'], y)

r :: QuestionID -> LetraRespuesta -> Respuesta
r x y = (x, y)

-- Test cases:
respuestas10 = ("s1", "t1", [r 0 'a', r 1 'b', r 2 'c'], "m1")
respuestas5 = ("s1", "t1", [r 0 'a', r 1 'b', r 2 'a'], "m2")
examen10 = ("t1", [p 0 'a', p 1 'b', p 2 'c'], [("m1", [0, 1, 2]), ("m2", [2, 1, 0])])
test10 = corrige examen10 respuestas10
test5 = corrige examen10 respuestas5
test6_25 = corrige ("t1", [p 0 'a', p 1 'b', p 2 'c', p 3 'a'], [("m2", [1, 2, 0, 3])]) ("s1", "t1", [ r 1 'b', r 2 'a', r 0 'a',r 3 'a'], "m2")

test_count_10 = conteos examen10 [respuestas10, respuestas5]
test_stats_0 = estadisticas examen10 [respuestas10, respuestas5]

testbench_base = [test10, test5, test6_25]
testbench_counting = [test_count_10]
testbench_stats = [test_stats_0]

test [] = []
test (x:xs) = x:(test xs)
